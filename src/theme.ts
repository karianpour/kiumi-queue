import { createMuiTheme } from '@material-ui/core';

const createTheme = (rtl: boolean) => createMuiTheme({
  direction: rtl ? 'rtl' : 'ltr',
  typography: {
    // Use the system font instead of the default Roboto font.
    // fontFamily: [
    //   'Yekan Bakh',
    // ].join(','),
  },
  overrides: {
    MuiContainer: {
      root:{
        // backgroundColor: COLORS.background,
      },
    },
    MuiTypography: {
      caption: {
        // color: COLORS.label,
      }
    },
    MuiPaper: {
      rounded: {
        // borderRadius: 8,
      },
      elevation1: {
        // boxShadow: '0 5px 10px 0 rgba(0, 0, 0, 0.08)',
      },
    },
    MuiPopover: {
      root:{
        flip: false,
        direction: rtl ? 'rtl' : 'ltr',
      },
    },
    MuiCssBaseline: {
      '@global': {
        html: {
          // backgroundColor: COLORS.background,
        },
        'html *': {
          // fontFamily: 'Yekan Bakh',
        },
      },
    },
  },
});

export default createTheme;

