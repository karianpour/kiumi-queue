import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  makeStyles, Typography,
} from '@material-ui/core';

const useStyles = makeStyles (theme => ({
  root: {
    display: 'flex',
    position: 'fixed',
    bottom: 0,
    width: '100%',
    padding: theme.spacing(1),
    marginTop: theme.spacing(2),
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.text.secondary,
  },
}));

const Footer: React.FunctionComponent<{}> = (props) => {
  const { t } = useTranslation();
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="body2">{t('copy_write')}</Typography>
    </div>
  )
};


export default Footer;