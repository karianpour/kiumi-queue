import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  makeStyles, NoSsr, Typography,
} from '@material-ui/core';
import { useHostState } from '../state/host-state';
import { observer } from 'mobx-react-lite';
import AddTrack from './AddTrack';
import { SearchStateProvider } from '../state/search-state';

const useStyles = makeStyles (theme => ({
  root: {
    position: 'sticky',
    top: 0,
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    maxWidth: 640,
    margin: `0 auto`,
    padding: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
  },
  spacer: {
    flexGrow: 1,
  },
}));

const HostInfo: React.FunctionComponent<{}> = observer((props) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const store = useHostState();

  const host = store.host;

  return (
    <div className={classes.root}>
      <Typography variant="caption">{t('host')}:&nbsp;</Typography>
      <Typography variant="body2">{host.name}</Typography>
      <div className={classes.spacer}/>
      <NoSsr>
        <SearchStateProvider>
          <AddTrack/>
        </SearchStateProvider>
      </NoSsr>
    </div>
  )
});


export default HostInfo;