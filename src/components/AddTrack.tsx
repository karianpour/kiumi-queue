import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  IconButton, Popover,
  makeStyles,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import TrackSearch from './TrackSearch';

const useStyles = makeStyles (theme => ({
  popover: {
    width: '100%',
    height: '100%',
    // maxWidth: 640,
  },
}));

const AddTrack: React.FunctionComponent = () => {
  const { t } = useTranslation();
  const classes = useStyles();

  const [ adding, setAdding ] = React.useState(false);

  const handleAddTrack = () => {
    setAdding(true);
  }

  const handleClose = () => {
    setAdding(false);
  }

  return (
    <>
      <IconButton color="inherit" aria-label={t('add_track')} onClick={handleAddTrack}>
        <AddIcon />
      </IconButton>
      <Popover 
        open={adding}
        anchorReference="anchorPosition"
        anchorPosition={{ top: 0, left: 0 }}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        classes={{
          paper: classes.popover,
        }}
        onClose={handleClose}
      >
        <TrackSearch onClose={handleClose}/>
      </Popover>
    </>
  )
};


export default AddTrack;