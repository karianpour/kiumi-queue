import React from 'react';
import {create} from 'jss';
import rtl from 'jss-rtl';
import { useTranslation } from 'react-i18next';
import { StylesProvider, jssPreset} from '@material-ui/core/styles';

const jss = create({plugins: [...jssPreset().plugins, rtl()]});

const Direction: React.FunctionComponent<{}> = (props) => {
  const { i18n } = useTranslation();
  const rtl = isRtl(i18n.language);

  if(!rtl){
    return <>
      {props.children}
    </>
  }

  return (
    <div dir="rtl">
      <StylesProvider jss={jss}>
          {props.children}
      </StylesProvider>
    </div>
  )
};

export default Direction;


export function isRtl(lang: string): boolean{
  if(lang === 'fa') return true;
  if(lang === 'ar') return true;
  return false;
}

type Position = 'right' | 'left';
type DirectionalPosition = 'start' | 'end';

export function directional(lang: string, position: DirectionalPosition): Position{
  return !isRtl(lang) ? position === 'start' ? 'left' : 'right' : position === 'start' ? 'right' : 'left';
}

