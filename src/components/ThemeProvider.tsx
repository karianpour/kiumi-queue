import React from 'react';
import {
  MuiThemeProvider,
 } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import createTheme from '../theme';
import { isRtl } from './Direction';

const ThemeProvider: React.FunctionComponent<{}> = (props) => {
  const { i18n } = useTranslation();
  const rtl = isRtl(i18n.language);

  return (
    <MuiThemeProvider theme={createTheme(rtl)}>
      {props.children}
    </MuiThemeProvider>
  )
};

export default ThemeProvider;
