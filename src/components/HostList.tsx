import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  makeStyles, Typography,
} from '@material-ui/core';
import { useHostState } from '../state/host-state';
import { observer } from 'mobx-react-lite';
import Track from './Track';

const useStyles = makeStyles (theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    maxWidth: 640,
    margin: `0 auto 48px auto`,
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.secondary,
  },
}));

const HostList: React.FunctionComponent<{}> = observer((props) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const store = useHostState();

  React.useEffect(()=>{
    store.populateTrackQueue();
  }, [store]);

  const queue = store.queue;

  return (
    <div className={classes.root}>
      <Typography variant="subtitle1">{t('play_list')}</Typography>
      {queue.map( (track, i) => (
        <Track key={`${track.id}-${i}`} track={track}/>
      ))}
    </div>
  )
});


export default HostList;