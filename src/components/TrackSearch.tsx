import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  InputBase, makeStyles, Typography, fade, IconButton,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import { observer } from 'mobx-react-lite';
import Track from './Track';
import { useSearchState } from '../state/search-state';
import { debounce } from '../utils';
import { useHostState } from '../state/host-state';

const useStyles = makeStyles (theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    maxWidth: 640,
    margin: `0 auto`,
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.secondary,
  },
  search: {
    display: 'flex',
    alignItems: 'center',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    padding: theme.spacing(0, 0.5),
    width: '100%',
  },
  searchIcon: {
    paddingRight: theme.spacing(1),
    height: '100%',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeIcon: {

  },
  inputRoot: {
    color: 'inherit',
    flexGrow: 1,
  },
  inputInput: {
    // padding: theme.spacing(1, 1, 1, 0),
  },
  track: {
    display: 'flex',
  },
  actions: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
}));

const TrackSearch: React.FunctionComponent<{onClose: () => void}> = observer(({ onClose }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const store = useSearchState();

  const handleSearch = debounce((query: string) => {
    store.query(query);
  }, 300);

  return (
    <div className={classes.root}>
      <div className={classes.search}>
        <div className={classes.searchIcon}>
          <SearchIcon />
        </div>
        <InputBase
          placeholder={t('search…')}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput,
          }}
          inputProps={{ 'aria-label': t('search') }}
          onChange={(event) => handleSearch(event.target.value)}
        />
        <IconButton className={classes.closeIcon} onClick={onClose} edge="end">
          <CloseIcon />
        </IconButton>
      </div>

      <SearchResult onClose={onClose}/>
    </div>
  )
});

export default TrackSearch;

const SearchResult: React.FC<{onClose: () => void}> = observer(({onClose}) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const state = useSearchState();
  const hostState = useHostState();

  const searchResult = state.searchResult;

  const handleSelect = async (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const { trackuri } = event.currentTarget.dataset;
    const result = await hostState.addTrack(trackuri);
    if(result){
      onClose();
      hostState.populateTrackQueue();
    }
  }

  return <>
    <Typography variant="subtitle1">{t('search_result')}</Typography>
    {searchResult.map( track => (
      <div key={track.id} className={classes.track}>
        <div className={classes.actions}>
          <IconButton onClick={handleSelect} data-trackuri={track.uri}><AddIcon/></IconButton>
        </div>
        <Track track={track}/>
      </div>
    ))}
  </>
})