import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  makeStyles, Typography,
} from '@material-ui/core';
import { ITrackState } from '../state/host-state';
import { observer } from 'mobx-react-lite';

const useStyles = makeStyles (theme => ({
  root: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column',
    width: '100%',
    maxWidth: 640,
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.primary,
  },
  trackInfo: {
    display: 'flex',
    alignItems: 'center',
  },
  image: {
    width: 48,
    height: 48,
    margin: theme.spacing(1),
  },
  desc: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
  audio: {
    alignSelf: 'flex-end',
  },
}));

const Track: React.FunctionComponent<{track: ITrackState}> = observer(({ track }) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const image = track.images.length > 0 && track.images[0];

  return (
    <div className={classes.root}>
      <div className={classes.trackInfo}>
        {image && <img src={image.url} alt={`image of ${track.name}`} width={image.width} height={image.height} className={classes.image}/>}
        <div className={classes.desc}>
          <Typography variant="subtitle1">{track.name}</Typography>
          <Typography variant="subtitle2">{track.artist} - {track.album}</Typography>
        </div>
      </div>
      {track.preview_url && <audio src={track.preview_url} controls className={classes.audio}>
        <Typography variant="body2" color="error">{t('audio_not_supported')}</Typography>
      </audio>}
    </div>
  )
});


export default Track;