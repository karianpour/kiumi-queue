import fa from './fa';
import en from './en';

export default {
  en, fa
}


export const defaultLng = 'en';
export const fallbackLng = 'en';