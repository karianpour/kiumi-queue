import { languages } from './languages'

export default {
  translation: {
    languages,
    'copy_write': 'All rights reserved by Kiumi',
    'audio_not_supported': 'Audio Not Supported',
    'host': 'Host',
    'play_list': 'Play List',
    'track': 'Track',
    'add_track': 'Add Track',
    'search': 'Search',
    'search…': 'Search…',
    'search_result': 'Search Result',
  }
}