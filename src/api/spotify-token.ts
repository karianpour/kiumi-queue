let data = {
  token: undefined,
  expires: undefined,
};

export async function getToken() {
  const now = new Date().getTime();
  if(data.token && data.expires > now){
    console.log('cached token')
    return data.token;
  }

  try{
    const params = {
      'grant_type': 'refresh_token',
      'refresh_token': process.env.REFRESH_TOKEN,
    }

    const result = await fetch('https://accounts.spotify.com/api/token', {
      method: 'POST',
      headers: {
        'Authorization': `Basic ${process.env.CLIENT_BASE64}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: encodeUrl(params),
    });

    const data = await result.json();
    data.token = data.access_token;
    data.expires = now + (data.expires_in - 600) * 1000;

    return data.token;
  }catch(err){
    console.log(err);
  }
}


export function encodeUrl(params: {[key: string]: string}){
  const formBody = [];
  for (let property in params) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(params[property]);
      formBody.push(encodedKey + "=" + encodedValue);
  }
  return formBody.join('&');
}
