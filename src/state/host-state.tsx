import React from 'react';
import { useLocalStore } from 'mobx-react-lite';
import { types, Instance, SnapshotIn, onSnapshot, getParentOfType, getEnv } from "mobx-state-tree";
import { IKiumiApi, KiumiApi } from '../api/kiumi-api';
import { type } from 'os';

export const ImageState = types
  .model("ImageState", {
    url: types.string,
    width: types.number,
    height: types.number,
  })

export const HostInfoState = types
  .model("HostInfoState", {
    id: types.string,
    name: types.string,
    images: types.array(ImageState),
  })
  .actions(self => {
    return {
      setName(name: string){
        self.name = name;
      },
      setImages(images: IImageState[]){
        self.images.clear();
        self.images.push(images);
      },
    }
  })

export const TrackState = types
  .model("TrackState", {
    id: types.string,
    name: types.string,
    preview_url: types.string,
    uri: types.string,
    album: types.string,
    artist: types.string,
    images: types.array(ImageState),
  })


export const HostState = types
  .model("HostState", {
    host: types.maybeNull(HostInfoState),
    queue: types.array(TrackState),
    currentTrackIndex: types.maybeNull(types.number),
  })
  .views(self => {
    return {
      // findCity(country: string, region: string, name: string): ICityState | undefined{
      //   const city = self.cities.get(`${country}/${region}/${name}`);
      //   return city;
      // },
    }
  })
  .actions(self => {
    return {
      setQueue(queue: ITrackState[]){
        self.queue.clear();
        self.queue.push(...queue);
      },
      setHost(host: IHostInfoState){
        self.host = host;
      },
    }
  })
  .actions(self => {
    return {
      async populateTrackQueue () {
        const queue = await getEnv<EnvType>(self).api.fetchQueue(self.host?.id);
        if(queue) {
          self.host.setName(queue.name);
          self.setQueue(queue.tracks.map( t => TrackState.create(t)));
        }
      },
      async addTrack(trackUri: string) {
        const result = await getEnv<EnvType>(self).api.fetchAddTrack(self.host?.id, trackUri);
        if(result){
          return true;
        }
      },
    }
  })

export const HostStoreContext = React.createContext<IHostState | null>(null);

export const HostStateProvider: React.FunctionComponent<{host: IHostInfoState}> = ({ host, children }) => {
  const store = useLocalStore(() => createStore(host))
  return <HostStoreContext.Provider value={store}>{children}</HostStoreContext.Provider>;
}

export const useHostState = () => {
  const store = React.useContext(HostStoreContext)
  if (!store) {
    // this is especially useful in TypeScript so you don't need to be checking for null all the time
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return store;
}

export type EnvType = {
  api: IKiumiApi,
}

export function createStore(host: IHostInfoState) {
  const injection: EnvType = {
    api: new KiumiApi(),
  }

  let store;
  try {
    if(process.browser){
      store = HostState.create(createSnapshot(host), injection);
    }else{
      store = HostState.create(emptyStore(host), injection);
    }
  } catch (err) {
    console.error('could not restore in create store, with the following error. fallback to empty store.');
    console.error(err);
    store = HostState.create(emptyStore(host), injection);
  }
  onSnapshot(store, saveSnapshot);

  return store;
}

export const SNAPSHOT_KEY = 'host-store';

export function createSnapshot(host: IHostInfoState): IHostStateSnapshot {
  try{
    const snapshot = localStorage?.getItem(`${SNAPSHOT_KEY}_${host.id}`);
    return snapshot ? JSON.parse(snapshot) : emptyStore(host);
  } catch (err) {
    console.error('could not restore, with the following error. fallback to empty store.');
    console.error(err);
    return emptyStore(host);
  }
}

export function saveSnapshot(snapshot: IHostStateSnapshot) {
  if(snapshot.host){
    localStorage?.setItem(`${SNAPSHOT_KEY}_${snapshot.host.id}`, JSON.stringify(snapshot));
  }
}

function emptyStore(host: IHostInfoState): IHostStateSnapshot {
  return {
    host,
  };
}

export interface IHostState extends Instance<typeof HostState> { };
export interface IHostStateSnapshot extends SnapshotIn<typeof HostState> { };
export interface ITrackState extends Instance<typeof TrackState> { };
export interface ITrackStateSnapshot extends SnapshotIn<typeof TrackState> { };
export interface IImageState extends Instance<typeof ImageState> { };
export interface IImageStateSnapshot extends SnapshotIn<typeof ImageState> { };
export interface IHostInfoState extends Instance<typeof HostInfoState> { };
export interface IHostInfoStateSnapshot extends SnapshotIn<typeof HostInfoState> { };
