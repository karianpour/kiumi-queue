import React from 'react';
import { useLocalStore } from 'mobx-react-lite';
import { types, Instance, SnapshotIn, onSnapshot, getParentOfType, getEnv } from "mobx-state-tree";
import { IKiumiApi, KiumiApi } from '../api/kiumi-api';
import { ITrackState, TrackState } from './host-state';

export const SearchState = types
  .model("SearchState", {
    search: types.maybeNull(types.string),
    searchResult: types.array(TrackState),
  })
  .views(self => {
    return {
      // findCity(country: string, region: string, name: string): ICityState | undefined{
      //   const city = self.cities.get(`${country}/${region}/${name}`);
      //   return city;
      // },
    }
  })
  .actions(self => {
    return {
      setSearchResult(result?: ITrackState[]){
        self.searchResult.clear();
        result && self.searchResult.push(...result);
      },
      setSearch(search: string){
        self.search = search;
      }
    }
  })
  .actions(self => {
    return {
      async query(query: string) {
        self.search = query;
        if(self.search){
          try{
            const result = await getEnv<EnvType>(self).api.fetchSearch(query);
            // self.setOnline();
            if(self.search === query){
              self.setSearchResult(result.map( t => TrackState.create(t)));
            }
          }catch(err){
            console.log({err})
            if(err.message === 'NetworkError'){
              // self.setOffline();
            }
          }
        }else{
          self.setSearchResult(undefined);
        }
      },
    }
  })

export const SearchStoreContext = React.createContext<ISearchState | null>(null);

export const SearchStateProvider: React.FunctionComponent = ({ children }) => {
  const store = useLocalStore(createStore);
  return <SearchStoreContext.Provider value={store}>{children}</SearchStoreContext.Provider>;
}

export const useSearchState = () => {
  const store = React.useContext(SearchStoreContext)
  if (!store) {
    // this is especially useful in TypeScript so you don't need to be checking for null all the time
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return store;
}

export type EnvType = {
  api: IKiumiApi,
}

export function createStore() {
  const injection: EnvType = {
    api: new KiumiApi(),
  }

  const store = SearchState.create(emptyStore(), injection);

  return store;
}

function emptyStore(): ISearchStateSnapshot {
  return {};
}

export interface ISearchState extends Instance<typeof SearchState> { };
export interface ISearchStateSnapshot extends SnapshotIn<typeof SearchState> { };
