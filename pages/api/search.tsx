import { NextApiRequest, NextApiResponse } from "next"
import { encodeUrl, getToken } from "../../src/api/spotify-token";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { q } = req.query;

  const token = await getToken();
  // curl -X "GET" "https://api.spotify.com/v1/search?q=adel&type=track" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer BQCb49HSNesSND_iRFHxPubi0Qi_Tjt6zhZdZj6CnUxQsz4DjyXM-JvrRmN_P3uB6jn56S7_Sg4BsQHEX3w"

  const params = {
    q: q.toString(),
    type:'track',
  }

  const result = await fetch(`https://api.spotify.com/v1/search?${encodeUrl(params)}`, {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  const data = await result.json();

  // console.log(data, data.tracks.items);

  const tracks = data.tracks.items.map( t => ({
    id: t.id,
    name: t.name,
    preview_url: t.preview_url || '',
    uri: t.uri,
    album: t.album?.name || 'unknown',
    artist: t.artists[0]?.name || 'unknown',
    images: t.album?.images || [],
  }));

  res.statusCode = 200;
  res.json(tracks);
}
