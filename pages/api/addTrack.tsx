import { NextApiRequest, NextApiResponse } from "next"
import { getToken } from "../../src/api/spotify-token";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { playListId, trackUri } = req.query;

  const token = await getToken();
    // curl -i -X POST "https://api.spotify.com/v1/playlists/3mxLVwun7DAODQA94oehZg/tracks?uris=spotify%3Atrack%3A4kflIGfjdZJW4ot2ioixTB" -H "Authorization: Bearer BQAvgA_e9ux1GbIQNLcH5QxHOSuXGvo-iAJgvf2PZrQW0DcGYHHTeG51SSqcZ6Zs2nfY4evJ_QoxWtURBHdYKnP_TlMU804LEFvXYclrMGAmVhtbLA3yhUxEh8coMezVp6EXklIPOXUctUjB_vTvFNI6t2yvhw5HuuTDRpeieLTgiAvHb1IEZJrIFUKdbEaTZUfh-823wHEHynlKqafIcKUXSWh0I2x9la4GIQ" -H "Accept: application/json"


  // const params = {
  //   q: q.toString(),
  //   type:'track',
  // }

  const result = await fetch(`https://api.spotify.com/v1/playlists/${playListId}/tracks?uris=${encodeURIComponent(trackUri.toString())}`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  const data = await result.json();

  res.statusCode = 200;
  res.json({
    ok: true,
  });
}
