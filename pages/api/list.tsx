import { NextApiRequest, NextApiResponse } from "next"
import { encodeUrl, getToken } from "../../src/api/spotify-token";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { id } = req.query;

  const token = await getToken();
  // curl -X GET "https://api.spotify.com/v1/playlists/3mxLVwun7DAODQA94oehZg" -H "Authorization: Bearer BQAvgA_e9ux1GbIQNLcH5QxHOSuXGvo-iAJgvf2PZrQW0DcGYHHTeG51SSqcZ6Zs2nfY4evJ_QoxWtURBHdYKnP_TlMU804LEFvXYclrMGAmVhtbLA3yhUxEh8coMezVp6EXklIPOXUctUjB_vTvFNI6t2yvhw5HuuTDRpeieLTgiAvHb1IEZJrIFUKdbEaTZUfh-823wHEHynlKqafIcKUXSWh0I2x9la4GIQ"
  //3mxLVwun7DAODQA94oehZg

  // const params = {
  //   q: q.toString(),
  //   type:'track',
  // }

  const result = await fetch(`https://api.spotify.com/v1/playlists/${id}`, {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  const data = await result.json();

  const name = data.name;
  const images = {
    height: data.images.height,
    width: data.images.width,
    url: data.images.url,
  };

  // console.log(data, data.tracks.items);

  const tracks = data.tracks.items.map( t => ({
    id: t.track.id,
    name: t.track.name,
    preview_url: t.track.preview_url || '',
    uri: t.track.uri,
    album: t.track.album?.name || 'unknown',
    artist: t.track.artists[0]?.name || 'unknown',
    images: t.track.album?.images || [],
  }));

  res.statusCode = 200;
  res.json({
    name,
    images,
    tracks,
  });
}
