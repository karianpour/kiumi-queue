// import 'mobx-react-lite/batchingForReactDom';
import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import {
  CssBaseline,
} from '@material-ui/core';
import Direction from '../src/components/Direction';
import ThemeProvider from '../src/components/ThemeProvider';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import resources, { defaultLng, fallbackLng } from '../src/locale';
import { AppProps } from 'next/app';

i18n
  .init({
    resources,
    lng: defaultLng,
    fallbackLng,

    interpolation: {
      escapeValue: false
    }
  });

export default function App(props: AppProps) {
  const { Component, pageProps } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    jssStyles?.parentElement?.removeChild(jssStyles);
  }, []);

  return (
    <>
      <Head>
        <title>Kiumi - Queue</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <I18nextProvider i18n={i18n}>
        <Direction>
          <ThemeProvider>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <Component {...pageProps} />
          </ThemeProvider>
        </Direction>
      </I18nextProvider>
    </>
  );
}

App.propTypes = {
  Component: PropTypes.elementType.isRequired,
  // pageProps: PropTypes.object.isRequired,
};
