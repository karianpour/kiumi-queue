import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheets } from '@material-ui/core/styles';
// import theme from '../src/theme';

export default class AppDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <link rel="icon" href="/favicon.ico" />
          {/* PWA primary color */}
          {/* <meta name="theme-color" content={theme.palette.primary.main} /> */}
          {/* <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          /> */}
{/*   
  <style jsx global>{`
            @font-face {
              font-family: Nahid;
              src: url('/assets/Nahid.eot');
              src: url('/assets/Nahid.eot?#iefix') format('embedded-opentype'),
                  url('/assets/Nahid.woff') format('woff'),
                  url('/assets/Nahid.ttf') format('truetype');
              font-weight: normal;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 01 Hairline.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 01 Hairline.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 01 Hairline.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 01 Hairline.ttf') format('truetype');
              font-weight: 100;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 02 Thin.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 02 Thin.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 02 Thin.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 02 Thin.ttf') format('truetype');
              font-weight: 200;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 03 Light.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 03 Light.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 03 Light.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 03 Light.ttf') format('truetype');
              font-weight: 300;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 04 Regular.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 04 Regular.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 04 Regular.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 04 Regular.ttf') format('truetype');
              font-weight: 400;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 05 Medium.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 05 Medium.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 05 Medium.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 05 Medium.ttf') format('truetype');
              font-weight: 500;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 06 Bold.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 06 Bold.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 06 Bold.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 06 Bold.ttf') format('truetype');
              font-weight: 700;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 07 Heavy.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 07 Heavy.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 07 Heavy.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 07 Heavy.ttf') format('truetype');
              font-weight: 800;
              font-display: swap;
            }
            @font-face {
              font-family: Yekan Bakh;
              src: url('/assets/fonts/Yekan Bakh Fa-En 08 Fat.eot');
              src: url('/assets/fonts/Yekan Bakh Fa-En 08 Fat.eot?#iefix') format('embedded-opentype'),
                  url('/assets/fonts/Yekan Bakh Fa-En 08 Fat.woff') format('woff'),
                  url('/assets/fonts/Yekan Bakh Fa-En 08 Fat.ttf') format('truetype');
              font-weight: 900;
              font-display: swap;
            }
          `}</style> */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

AppDocument.getInitialProps = async (ctx) => {
  // Resolution order
  //
  // On the server:
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. document.getInitialProps
  // 4. app.render
  // 5. page.render
  // 6. document.render
  //
  // On the server with error:
  // 1. document.getInitialProps
  // 2. app.render
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. app.render
  // 4. page.render

  // Render app and page and get the context of the page with collected side effects.
  const sheets = new ServerStyleSheets();
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
    });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
  };
};