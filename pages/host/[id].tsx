import React from 'react';

import { 
  // makeStyles,
  NoSsr,
} from '@material-ui/core';
import { GetServerSideProps } from 'next';
import { fetchHost } from '../../src/api/kiumi-api';
import Footer from '../../src/components/Footer';
import HostList from '../../src/components/HostList';
import { HostStateProvider, IHostInfoState } from '../../src/state/host-state';
import HostInfo from '../../src/components/HostInfo';

// const useStyles = makeStyles (theme => ({
//   root: {
//     // position: 'absolute',
//     // top: '50%',
//     // left: '50%',
//     // '-ms-transform': 'translate(-50%, -50%)',
//     // transform: 'translate(-50%, -50%)',
//     // width: 646,
//     // [theme.breakpoints.down('sm')]: {
//     //   // width: 'unset',
//     //   // maxWidth: 312,
//     //   width: 312,
//     // },
//   },
// }));

const Host: React.FunctionComponent<{host: IHostInfoState}> = ({host}) => {
  // const classes = useStyles();

  return (
    <>
      <HostStateProvider host={host}>
        <HostInfo/>
        <NoSsr>
          <HostList/>
        </NoSsr>
        <Footer />
      </HostStateProvider>
    </>
  )
};

export default Host;

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { id } = ctx.query;

  const host = id && await fetchHost(typeof id === 'string' ? id : id[0]);

  return {
    props: {
      host,
    },
  };
}